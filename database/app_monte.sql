-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2018 at 11:48 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_monte`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_full_details`
--

CREATE TABLE `tbl_admin_full_details` (
  `id` int(11) NOT NULL,
  `full_name` varchar(5000) NOT NULL,
  `phone` varchar(1000) NOT NULL,
  `designation` varchar(5000) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `login_id` int(11) NOT NULL,
  `pp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `id` int(11) NOT NULL,
  `class_name` varchar(5000) NOT NULL,
  `enrolled_year` int(11) NOT NULL,
  `class_nick_name` varchar(5000) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0->Passout 1->Running'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`id`, `class_name`, `enrolled_year`, `class_nick_name`, `status`) VALUES
(1, 'Mountain A', 2018, 'Snow 1', 1),
(2, 'Mountain B', 2018, 'Snow 2', 1),
(3, 'Rock A', 2016, 'Rocket 1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feeds`
--

CREATE TABLE `tbl_feeds` (
  `id` int(11) NOT NULL,
  `feed_description` varchar(5000) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attached_photo_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'posted_by'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_feeds`
--

INSERT INTO `tbl_feeds` (`id`, `feed_description`, `post_date`, `attached_photo_id`, `teacher_id`) VALUES
(1, 'Yeppieeee!!!', '2018-08-09 09:00:29', 1, 1),
(2, 'BoooYaaaaahhhh!!!', '2018-08-09 09:01:10', 2, 1),
(3, 'They are very adorable little things. ', '2018-08-10 09:33:58', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id` int(11) NOT NULL,
  `uname` varchar(5000) NOT NULL,
  `pwd` varchar(5000) NOT NULL,
  `access_type` tinyint(4) NOT NULL COMMENT '1->Admin / 2->Teacher / 3->Parent/Student'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `uname`, `pwd`, `access_type`) VALUES
(1, 'ram', '4641999a7679fcaef2df0e26d11e3c72', 2),
(2, 'stud1', '4641999a7679fcaef2df0e26d11e3c72', 3),
(3, 'stud2', '4641999a7679fcaef2df0e26d11e3c72', 3),
(4, 'stud3', '4641999a7679fcaef2df0e26d11e3c72', 3),
(5, 'stud4', '4641999a7679fcaef2df0e26d11e3c72', 3),
(6, 'stud5', '4641999a7679fcaef2df0e26d11e3c72', 3),
(7, 'stud6', '4641999a7679fcaef2df0e26d11e3c72', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_from_students`
--

CREATE TABLE `tbl_message_from_students` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL COMMENT 'message_from',
  `message` varchar(5000) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'message_to'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_from_teachers`
--

CREATE TABLE `tbl_message_from_teachers` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'message_from',
  `message` varchar(5000) NOT NULL,
  `student_id` int(11) NOT NULL COMMENT 'message_to'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notices`
--

CREATE TABLE `tbl_notices` (
  `id` int(11) NOT NULL,
  `notice_description` varchar(5000) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'notice_posted_by',
  `published_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notices`
--

INSERT INTO `tbl_notices` (`id`, `notice_description`, `teacher_id`, `published_date`, `end_date`) VALUES
(4, ' M.B High School, Mumbai.\r\nNOTICE\r\n\r\n30 July, 2014\r\n\r\nDRAMATICS COMPETITION – AUDITION\r\n\r\nAn Inter School Dramatics Competition will be held on 31-03-2018 at Nalanda Hall. An audition will be conducted for selecting students for the school team. Interested candidates can give their names to the undersigned. The details are given below:\r\n\r\nDate: 07-08-2014\r\n\r\nTime : 10.30 am\r\n\r\nVenue: School Auditorium\r\n\r\nEligibility: Class IX and XII\r\n\r\nLast date for giving names: 15-03-2018\r\n\r\nRoopa\r\n\r\nCultural Secretary', 1, '2018-08-12 09:30:02', '16 August, 2018'),
(5, '\r\n            M R V  PUBLIC SCHOOL, HYDERABAD\r\n                                   NOTICE\r\n30 July 2016\r\n            DRAMATICS COMPETITION - AUDITION\r\nAn Inter-school Dramatics Competition will be held on 30-08-2014 at Nalanda Hall. An audition will be held to select students for the school team. Interested candidates may give their names to the undersigned. The details of the audition are given below:\r\nDate: 07-08-2014\r\nTime: 10.30 am\r\nVenue: School Auditorium\r\nEligibility: Students of classes IX  and XII\r\nLast date for giving names: 05-08-2014\r\nRoopa,\r\nCultural Secretary', 1, '2018-08-12 09:31:35', '20 September, 2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_photos`
--

CREATE TABLE `tbl_photos` (
  `id` int(11) NOT NULL,
  `photo_name` varchar(5000) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_photos`
--

INSERT INTO `tbl_photos` (`id`, `photo_name`, `upload_date`) VALUES
(1, '153380522926980.jpg', '2018-08-09 09:00:29'),
(2, '15338052701997.png', '2018-08-09 09:01:10'),
(3, '153389363827332.jpg', '2018-08-10 09:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school_details`
--

CREATE TABLE `tbl_school_details` (
  `id` int(11) NOT NULL,
  `school_name` varchar(5000) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `phone` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_school_details`
--

INSERT INTO `tbl_school_details` (`id`, `school_name`, `address`, `phone`) VALUES
(1, '2018-HAPPY-CLUB', 'KTM, KATHMANDU, NEPAL', '01-4037699');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_full_details`
--

CREATE TABLE `tbl_student_full_details` (
  `id` int(11) NOT NULL,
  `full_name` varchar(5000) NOT NULL,
  `fateher_name` varchar(5000) NOT NULL,
  `mothername` varchar(5000) NOT NULL,
  `father_phone` varchar(1000) NOT NULL,
  `mother_phone` varchar(1000) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `email` varchar(5000) NOT NULL,
  `class_id` int(11) NOT NULL,
  `loginid` int(11) NOT NULL,
  `pp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_full_details`
--

INSERT INTO `tbl_student_full_details` (`id`, `full_name`, `fateher_name`, `mothername`, `father_phone`, `mother_phone`, `address`, `email`, `class_id`, `loginid`, `pp_id`) VALUES
(1, 'Liam', 'Liam Father', 'Liam Mother', '+977-9876543210', '+977-9874651230', 'w023Brob0A', 'ADIZYgdYT3', 1, 2, 5),
(2, 'bU3ZItbBvl', 'kwwTErKjgW', 'VDi6IX6n5y', 'HhXJPpVO8S', 'kpzgVsC9sH', 'l1LcQxby5C', 'Ca1ZSHwkKJ', 1, 3, 7),
(3, 'NbOvT3RQfx', 'GHcI9LkL8a', '4gKQoanPjc', 'wIB1NXuxws', '2uF9JOzCcr', 'clJ43N0OLI', 'iNM4gq7WPb', 2, 4, 10),
(4, 'BnSctCbG92', '0OjxmoUdXW', 'lb0Ubr6npD', '33wuuLUKNv', 'ssv2W6dAY5', 'JvVBYI2FRf', 'BMZPePwtSc', 2, 5, 11),
(5, 'v75NLBRrgN', 'XwF1Mo0zlu', 'x4JP6gzgU8', 'bWXPcedyiX', 'SkvykT7L1x', 'l1z3xCDIF1', 'CTAuvELaHY', 3, 6, 12),
(6, 'Elbmk3yeRh', 'YOu2fMLzHL', 'aGM8ktCVMT', 'dyUghhXHzP', 'My8FsZUcMY', 'STpce6vJGB', 'a6QzD4TQ8b', 3, 7, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag_logs`
--

CREATE TABLE `tbl_tag_logs` (
  `id` int(11) NOT NULL,
  `feed_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tag_logs`
--

INSERT INTO `tbl_tag_logs` (`id`, `feed_id`, `student_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 2, 4),
(5, 3, 1),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher_full_details`
--

CREATE TABLE `tbl_teacher_full_details` (
  `id` int(11) NOT NULL,
  `full_name` varchar(5000) NOT NULL,
  `phone` varchar(1000) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `login_id` int(11) NOT NULL,
  `pp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teacher_full_details`
--

INSERT INTO `tbl_teacher_full_details` (`id`, `full_name`, `phone`, `address`, `login_id`, `pp_id`) VALUES
(1, 'Rameshor Charausi', '9846552255', 'KTM', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_full_details`
--
ALTER TABLE `tbl_admin_full_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_feeds`
--
ALTER TABLE `tbl_feeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_message_from_students`
--
ALTER TABLE `tbl_message_from_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_message_from_teachers`
--
ALTER TABLE `tbl_message_from_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notices`
--
ALTER TABLE `tbl_notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_school_details`
--
ALTER TABLE `tbl_school_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_full_details`
--
ALTER TABLE `tbl_student_full_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tag_logs`
--
ALTER TABLE `tbl_tag_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teacher_full_details`
--
ALTER TABLE `tbl_teacher_full_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_full_details`
--
ALTER TABLE `tbl_admin_full_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_feeds`
--
ALTER TABLE `tbl_feeds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_message_from_students`
--
ALTER TABLE `tbl_message_from_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_message_from_teachers`
--
ALTER TABLE `tbl_message_from_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notices`
--
ALTER TABLE `tbl_notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_school_details`
--
ALTER TABLE `tbl_school_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_student_full_details`
--
ALTER TABLE `tbl_student_full_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_tag_logs`
--
ALTER TABLE `tbl_tag_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_teacher_full_details`
--
ALTER TABLE `tbl_teacher_full_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
