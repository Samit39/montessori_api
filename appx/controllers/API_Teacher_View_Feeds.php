<?php

require(APPPATH.'/libraries/REST_Controller.php');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class API_Teacher_View_Feeds extends REST_Controller{

    public function __construct($config = 'rest')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct($config);
        $this->load->model('Feed_model');
    }

    //API - Get Running Class From Database [Tbl_class]
    function getAllMyFeeds_get()
    {
        $appid=$this->get('appid');
        $teacherid=$this->get('teacherid');
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            $result = $this->Feed_model->getAllMyFeeds($teacherid);
            if($result)
            {
               // $this->response($result, 200); 
                // RESULT OK 
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else
            {
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'Invalid Parameters'));
                exit;
            }
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }
}