<?php
class Chat_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function getAllRunningClassStudentsAndTeachers($teacherid)
  {
  	$this->db->select('s.id as student_id,s.full_name as student_fullname,p.photo_name as profilepicture,s.class_id,c.class_name,c.enrolled_year');
    $this->db->from('tbl_student_full_details s');
    $this->db->join('tbl_class c','c.id=s.class_id');
    $this->db->join('tbl_photos p','p.id=s.pp_id');
    $this->db->where('c.status',1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }

  public function getSingleStudentDetailsFromId($studentid)
  {
    $this->db->select('*');
    $this->db->from('tbl_student_full_details s');
    $this->db->where('s.id',$studentid);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }


  public function getTeacherChatDetails($senderid,$receiverid)
  {
    $res1=$this->getChatDetailsFromTblMessageFromStudents($senderid,$receiverid);
    $res2=$this->getChatDetailsFromTblMessageFromTeachers($senderid,$receiverid);
    $res3=array_merge($res1,$res2);
    // echo json_encode($res3);

    //Create index rows
  foreach ($res3 as $row) {
    foreach ($row as $key => $value){
      ${$key}[]  = $value; //Creates $receiveddate, $teacher_id type arrays
    }  
  }
  array_multisort($messagedate, SORT_ASC, $res3);
  return $res3;
  }

  public function getChatDetailsFromTblMessageFromStudents($senderid,$receiverid)
  {
    $this->db->select('FS.message as receivedmessage,FS.received_date as messagedate,P.photo_name as studentPP');
    $this->db->from('tbl_message_from_students FS');
    $this->db->join('tbl_student_full_details D','D.id=FS.student_id');
    $this->db->join('tbl_photos P','P.id=D.pp_id');
    $this->db->where('FS.student_id',$senderid);
    $this->db->where('FS.teacher_id',$receiverid);
    $this->db->order_by("FS.received_date", "desc");
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }

  public function getChatDetailsFromTblMessageFromTeachers($senderid,$receiverid)
  {
    $this->db->select('FT.message as sentmessage,FT.received_date as messagedate,P.photo_name as teacherPP');
    $this->db->from('tbl_message_from_teachers FT');
    $this->db->join('tbl_teacher_full_details D','D.id=FT.teacher_id');
    $this->db->join('tbl_photos P','P.id=D.pp_id');
    $this->db->where('FT.teacher_id',$senderid);
    $this->db->where('FT.student_id',$receiverid);
    $this->db->order_by("FT.received_date", "desc");
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }
}