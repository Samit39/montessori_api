<?php

require(APPPATH.'/libraries/REST_Controller.php');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
 
class API_Login extends REST_Controller{
    
    public function __construct($config = 'rest')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        parent::__construct($config);

        $this->load->model('Login_model');
    }

    //API to get school details from database
    function getSchoolDetails_get()
    {
        $appid=$this->get('appid');
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            $result = $this->Login_model->getSchoolDetails();
            if($result)
            {
               // $this->response($result, 200); 
                // RESULT OK 
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else
            {
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'Invalid Parameters'));
                exit;
            }
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }

    //API - app sends uname and password and on valid login, information is sent back
    function checklogin_get(){
        $appid=$this->get('appid');
        $uname  = $this->get('username');
        $pwd=md5($this->get('password'));
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
           if(!$uname || !$pwd){
                // $this->response("No Username Or Password Specified", 400);
                //EMPTY SENT
                echo json_encode(array("status"=>400,"result"=>'none'));
                exit;
            }

            $result = $this->Login_model->check_valid_login( $uname,$pwd );

            if($result){

                // $this->response($result, 200); 
                // RESULT OK 
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else{
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'none'));
                exit;
            } 
        }
        else
        {
            // $this->response("Invalid API Key",404);
            //INVALID API KEY
            echo json_encode(array("status"=>401,"result"=>'none'));
            exit;
        }
        
    } 

    //API - app sends loginid and on valid id, teacher information is sent back
    function getTeacherInfo_get(){
        $appid=$this->get('appid');
        $loginid=$this->get('loginid');
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
           if(!$loginid){
                //EMPTY PARAMETERS
                echo json_encode(array("status"=>400,"result"=>'none'));
                exit;
            }

            $result = $this->Login_model->getTeacherInfo( $loginid);

            if($result){

                // $this->response($result, 200); 
                //RESULT OK
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else{
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'none'));
                exit;
            } 
        }
        else
        {
            // $this->response("Invalid API Key",404);
            // INVALID API KEY
            echo json_encode(array("status"=>401,"result"=>'none'));
            exit;
        }
        
    }


}