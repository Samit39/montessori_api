<?php

require(APPPATH.'/libraries/REST_Controller.php');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class API_Upload_Feed extends REST_Controller{

    public function __construct($config = 'rest')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        parent::__construct($config);

        $this->load->model('Feed_model');
    }
    //API - Get Running Class From Database [Tbl_class]
    function getAllRunningClass_get()
    {
        $appid=$this->get('appid');
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            $result = $this->Feed_model->getAllRunningClass();
            if($result)
            {
               // $this->response($result, 200); 
                // RESULT OK 
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else
            {
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'none'));
                exit;
            }
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }

    //API - Upload a Picture Feed To Database - 3 Table operation [Tbl_photos,Tbl_feeds,Tbl_tags]
    function addfeed_post()
    {
        $appid=$this->post('appid');
        $student_ids_tags=$this->post('tag_ids');
        $teacher_id=$this->post('teacherid');
        $feed_desc=$this->post('feed_desc');
        $photo_name=$this->post('photo_name');
        // echo json_encode(array("tags"=>$student_ids_tags));
        // exit;
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            if(!$student_ids_tags || !$teacher_id || !$feed_desc)
            {
                //EMPTY SENT
                echo json_encode(array("status"=>400,"result"=>'none'));
                exit;
            }
            else
            {
                //PHOTO UPLOADING SECTION STARTS
                if(!empty($_FILES["photo_name"]["name"]))
                {
                    //IF FILE IS SELECTED
                    $uploaddir = './uploads/totalphotos/';
                    $path = $_FILES['photo_name']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    if(strtolower($ext)=="png"||strtolower($ext)=="jpg"||strtolower($ext)=="jpeg"||strtolower($ext)=="gif")
                    {
                        //IF EXTENSION IS CORRECT
                        $user_img = time() . rand() . '.' . $ext;
                        $uploadfile = $uploaddir . $user_img;

                        if (move_uploaded_file($_FILES['photo_name']['tmp_name'], $uploadfile)) 
                        {
                            //UPLOAD SUCCESS
                            $result = $this->Feed_model->add_new_feed($student_ids_tags,$teacher_id,$feed_desc,$user_img);
                            if($result){
                                // $this->response($result, 200); 
                                // 200 (RESULT OK) 
                                echo json_encode(array("status"=>200,"result"=>$result));
                                exit;
                            } 
                            else{
                                // $this->response("Invalid ISBN", 404);
                                //404 (INVALID PARAMETERS)
                                echo json_encode(array("status"=>404,"result"=>'Invalid Parameters'));
                                exit;
                            } 
                        } 
                        else 
                        {
                            //UPLOAD FAILED
                            //500 (Internal Server Error)
                            echo json_encode(array("status"=>500,"result"=>'Upload Failed, Internal Server Error'));
                            exit;        
                        }
                    }
                    else
                    {
                        //IF EXTENSION IS INVALID
                        //415 (Unsupported Media Type)
                        echo json_encode(array("status"=>415,"result"=>'Unsupported Media Type'));
                        exit;
                    }
                    
                }
                else
                {
                    //IF FILE IS NOT SELECTED
                    //204 (No Content)
                    echo json_encode(array("status"=>204,"result"=>'File Not Selected'));
                    exit;
                }
                //PHOTO UPLOADING SECTION ENDS
            } 
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }
    function addBook_post(){

       $name      = $this->post('name');

       $price     = $this->post('price');

       $author    = $this->post('author');

       $category  = $this->post('category');

       $language  = $this->post('language');

       $isbn      = $this->post('isbn');

       $pub_date  = $this->post('publish_date');

       if(!$name || !$price || !$author || !$price || !$isbn || !$category){

        $this->response("Enter complete book information to save", 400);

    }else{

        $result = $this->book_model->add(array("name"=>$name, "price"=>$price, "author"=>$author, "category"=>$category, "language"=>$language, "isbn"=>$isbn, "publish_date"=>$pub_date));

        if($result === 0){

            $this->response("Book information coild not be saved. Try again.", 404);

        }else{

            $this->response("success", 200);  

        }

    }

}



    //API - app sends uname and password and on valid login, information is sent back
function checklogin_get(){
    $appid=$this->get('appid');
    $uname  = $this->get('username');
    $pwd=md5($this->get('password'));
    if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
    {
     if(!$uname || !$pwd){
                // $this->response("No Username Or Password Specified", 400);
                //EMPTY SENT
        echo json_encode(array("status"=>400,"result"=>'none'));
        exit;
    }

    $result = $this->Login_model->check_valid_login( $uname,$pwd );

    if($result){

                // $this->response($result, 200); 
                // RESULT OK 
        echo json_encode(array("status"=>200,"result"=>$result));
        exit;
    } 
    else{
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
        echo json_encode(array("status"=>404,"result"=>'none'));
        exit;
    } 
}
else
{
            // $this->response("Invalid API Key",404);
            //INVALID API KEY
    echo json_encode(array("status"=>401,"result"=>'none'));
    exit;
}

} 

    //API - app sends loginid and on valid id, teacher information is sent back
function getTeacherInfo_get(){
    $appid=$this->get('appid');
    $loginid=$this->get('loginid');
    if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
    {
     if(!$loginid){
                //EMPTY PARAMETERS
        echo json_encode(array("status"=>400,"result"=>'none'));
        exit;
    }

    $result = $this->Login_model->getTeacherInfo( $loginid);

    if($result){

                // $this->response($result, 200); 
                //RESULT OK
        echo json_encode(array("status"=>200,"result"=>$result));
        exit;
    } 
    else{
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
        echo json_encode(array("status"=>404,"result"=>'none'));
        exit;
    } 
}
else
{
            // $this->response("Invalid API Key",404);
            // INVALID API KEY
    echo json_encode(array("status"=>401,"result"=>'none'));
    exit;
}

}


}