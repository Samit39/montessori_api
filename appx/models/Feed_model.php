<?php
class Feed_model extends CI_Model {

  public function __construct(){

    $this->load->database();

  }
//Below This Are For API Teacher View Feeds ---------------------------------------------
  public function getAllMyFeeds($teacherid)
  {
    $this->db->select('
      f.id, 
      f.feed_description,
      f.post_date,
      p.photo_name
      ');
    $this->db->from('tbl_feeds f');
    $this->db->join('tbl_photos p','p.id=f.attached_photo_id');
    $this->db->where('f.teacher_id',$teacherid);
    $this->db->order_by("post_date", "desc");
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }

  }
//Below This Are For API Upload Feed ----------------------------------------------------
//Function for getting all running class
  public function getAllRunningClass()
  {
    $this->db->select('*');
    $this->db->from('tbl_class');
    $this->db->order_by("enrolled_year", "desc");
    $this->db->where('status',1); 
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }

//Function for adding new feed
  public function add_new_feed($student_ids_tags,$teacherid,$feed_desc,$imagename)
  {
        //Insert to tbl_photos
    if($this->insert_to_tbl_photos($imagename)==true)
    {
          //Insert to tbl photos succeced
      $photoid=$this->get_photo_id_by_name($imagename);

          //Insert to tbl feeds
      if($this->insert_to_tbl_feeds($feed_desc,$photoid,$teacherid)==true)
      {
            //Insert to tbl feeds succeded
        $feedid=$this->get_feed_id_by_feed_desc_photoid_techerid($feed_desc,$photoid,$teacherid);

            //Insert to tbl_tag_logs
        if($this->insert_to_tbl_tag_logs($feedid,$student_ids_tags)==true)
        {
              //Insert to tbl_tag_logs_succeded
          return true;
        }
        else
        {
              //Insert to tbl_tag_logs failed
          return false;
        }
      }
      else
      {
            //Insert to tbl feeds failed
        return false;
      }
    }
    else
    {
          //Insert to tbl photos failed
      return false;
    }
  }

//Associated Functions To [function addnewfeed] starts-----------------------------------
  public function insert_to_tbl_photos($imagename)
  {
    $data['photo_name']=$imagename;

    if($this->db->insert('tbl_photos', $data))
    {
     return true;
   }
   else
   {
     return false;

   }
 }

 public function get_photo_id_by_name($imagename)
 {
  $this->db->select('id');
  $this->db->from('tbl_photos');
  $this->db->where('photo_name',$imagename);
  return $this->db->get()->row()->id;
}

public function insert_to_tbl_feeds($feed_desc,$photoid,$teacherid)
{
  $data['feed_description']=$feed_desc;
  $data['attached_photo_id']=$photoid;
  $data['teacher_id']=$teacherid;
  if($this->db->insert('tbl_feeds', $data))
  {
   return true;
 }
 else
 {
   return false;

 }
}

public function get_feed_id_by_feed_desc_photoid_techerid($feed_desc,$photoid,$teacherid)
{
  $this->db->select('id');
  $this->db->from('tbl_feeds');
  $this->db->where('feed_description',$feed_desc);
  $this->db->where('attached_photo_id',$photoid);
  $this->db->where('teacher_id',$teacherid);
  return $this->db->get()->row()->id;
}

public function insert_to_tbl_tag_logs($feedid,$total_student_ids_tags)
{
        // $no_of_tags=count($student_ids_tags);
  $data_insert_count=0;
  // $student_ids_tags=explode(",", $total_student_ids_tags);
  $student_ids_tags=$total_student_ids_tags;
  $no_of_tags=count($student_ids_tags);

  for ($i=0; $i < $no_of_tags; $i++) 
  { 
    if($this->tag_all_students_of_this_class($student_ids_tags[$i],$feedid))
    {
      $data_insert_count++;
    }
    else
    {
      return false;
    }
  }
  if($no_of_tags==$data_insert_count)
  {
    return true;
  }
  else
  {
    return false;
  }
}

public function tag_all_students_of_this_class($classid,$feedid)
{
  $all_students=$this->get_all_students_of_this_class($classid);
  $no_of_students=count($all_students);
  $data_insert_count=0;
  for ($i=0; $i <$no_of_students ; $i++) 
  {
   $data['feed_id']=$feedid;
   $data['student_id']=$all_students[$i];
   if($this->db->insert('tbl_tag_logs', $data))
   {
     $data_insert_count++;
   }
   else
   {
     return false;
   }
 }
 if($data_insert_count==$no_of_students)
 {
  return true;
}
else
{
  return false;
}
}

public function get_all_students_of_this_class($classid)
{
  $this->db->select('id');
  $this->db->from('tbl_student_full_details');
  $this->db->where('class_id',$classid); 
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $result=$query->result_array();
    $array = array();
    foreach ( $result as $key => $val )
    {
      $temp = array_values($val);
      $array[] = $temp[0];
    }
    return $array;
  }
  else
  {
    return 0;
  }
}
//Associated Functions To [function addnewfeed] ends ------------------------------------



//----------------------------------------------------------------
public function check_valid_login($username,$password)
{
  $this->db->select('id,access_type');
  $this->db->from('tbl_login');
  $this->db->where('uname',$username);
  $this->db->where('pwd',$password);

  $query=$this->db->get();

  if($query->num_rows()==1)
  {
    return $query->result_array();
  }
  else
  {
    return 0;
  }
}

public function getTeacherInfo( $loginid)
{
  $this->db->select('td.*');
  $this->db->from('tbl_teacher_full_details td');
  $this->db->join('tbl_login l','td.login_id=l.id','left');
  $this->db->where('td.login_id',$loginid);

  $query=$this->db->get();

  if($query->num_rows()==1)
  {
    return $query->result_array();
  }
  else
  {
    return 0;
  }
}

      //API call - get a book record by isbn
public function getbookbyisbn($isbn){  

 $this->db->select('id, name, price, author, category, language, ISBN, publish_date');

 $this->db->from('tbl_books');

 $this->db->where('isbn',$isbn);

 $query = $this->db->get();

 if($query->num_rows() == 1)
 {

   return $query->result_array();

 }
 else
 {

   return 0;

 }

}

    //API call - get all books record
public function getallbooks(){   

  $this->db->select('id, name, price, author, category, language, ISBN, publish_date');

  $this->db->from('tbl_books');

  $this->db->order_by("id", "desc"); 

  $query = $this->db->get();

  if($query->num_rows() > 0){

    return $query->result_array();

  }else{

    return 0;

  }

}

   //API call - delete a book record
public function delete($id){

 $this->db->where('id', $id);

 if($this->db->delete('tbl_books')){

  return true;

}else{

  return false;

}

}

   //API call - add new book record
public function add($data){

  if($this->db->insert('tbl_books', $data)){

   return true;

 }else{

   return false;

 }

}

    //API call - update a book record
public function update($id, $data){

 $this->db->where('id', $id);

 if($this->db->update('tbl_books', $data)){

  return true;

}else{

  return false;

}

}

}