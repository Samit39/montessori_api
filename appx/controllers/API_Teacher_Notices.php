<?php

require(APPPATH.'/libraries/REST_Controller.php');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class API_Teacher_Notices extends REST_Controller{

    public function __construct($config = 'rest')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct($config);
        $this->load->model('Notices_model');
    }

    //API - Get All Notices From Database [Tbl_notice]
    function getAllNotices_get()
    {
        $appid=$this->get('appid');
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            $result = $this->Notices_model->getAllMyFeeds();
            if($result)
            {
               // $this->response($result, 200); 
                // RESULT OK 
                echo json_encode(array("status"=>200,"result"=>$result));
                exit;
            } 
            else
            {
                // $this->response("Invalid ISBN", 404);
                //INVALID PARAMETERS
                echo json_encode(array("status"=>404,"result"=>'Invalid Parameters'));
                exit;
            }
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }

    //API - Upload a Notice Feed To Database - tbl-notices
    function addnotice_post()
    {
        $appid=$this->post('appid');
        $teacher_id=$this->post('teacherid');
        $notice_desc=$this->post('notice_desc');
        $notice_expiry_date=$this->post('notice_expiry_date');
        // echo json_encode(array("tags"=>$student_ids_tags));
        // exit;
        if($appid=="764df61a8dcd29d91207056e44f77a4714a13688")
        {
            if(!$notice_expiry_date || !$teacher_id || !$notice_desc)
            {
                //EMPTY SENT
                echo json_encode(array("status"=>400,"result"=>'EMPTY PARAMETERS!!!'));
                exit;
            }
            else
            {
                $result = $this->Notices_model->add_new_notice($teacher_id,$notice_desc,$notice_expiry_date);
                if($result)
                {
                    // $this->response($result, 200); 
                    // 200 (RESULT OK) 
                    echo json_encode(array("status"=>200,"result"=>$result));
                    exit;
                } 
                else
                {
                // $this->response("Invalid ISBN", 404);
                //404 (INVALID PARAMETERS)
                    echo json_encode(array("status"=>404,"result"=>'Invalid Parameters'));
                    exit;
                } 
            } 
        }
        else
        {
            //INVALID API KEY
            //401 (Unauthorized)
            echo json_encode(array("status"=>401,"result"=>'Unauthorized API Key'));
            exit;
        }
    }
}