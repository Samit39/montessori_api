<?php
class Notices_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function getAllMyFeeds()
	{
		$this->db->select('n.*,t.full_name');
		$this->db->from('tbl_notices n');
    	$this->db->join('tbl_teacher_full_details t','t.id=n.teacher_id');
		$this->db->order_by("published_date", "desc");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return 0;
		}
	}

	public function add_new_notice($teacher_id,$notice_desc,$notice_expiry_date)
	{
		$data['notice_description']=$notice_desc;
		$data['teacher_id']=$teacher_id;
		$data['end_date']=$notice_expiry_date;
		if($this->db->insert('tbl_notices', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
