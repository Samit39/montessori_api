<?php
class Gallery_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function getAllPhotos()
  {
  	$this->db->select('*');
    $this->db->from('tbl_photos');
    $this->db->order_by("upload_date", "desc");
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      return $query->result_array();
    }
    else
    {
      return 0;
    }
  }
}